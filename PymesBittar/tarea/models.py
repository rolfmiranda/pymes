from django.db import models

# Create your models here.
class tarea(models.Model):
    nombre = models.CharField(max_length=200, null=True, blank=True)
    descripcion = models.CharField(max_length=200, unique=True, null=True, blank=True)
    fecha_entrega = models.CharField(max_length=200)
    hora_entrega = models.CharField(max_length=200)
    responsable = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    created_hour = models.TimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    
    class meta:
        Verbose_name = 'tarea'
        Verbose_name_plural = 'tareas'
        order_with_respect_to = 'fecha_entrega'
    
    def ___str__(self):
        return self.nombre